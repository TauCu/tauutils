package me.taucu.tauutils;

import me.taucu.tauutils.chronology.TimeFormatter;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class TimeFormatterTest {

    @Test
    public void standardTimeFormatterTest() {
        String formatted = TimeFormatter.STANDARD.format(TimeUnit.NANOSECONDS, 10_050_012_050L, TimeUnit.SECONDS, TimeUnit.MICROSECONDS);
        Assert.assertEquals(formatted, "10s, 50ms, 12us");
    }

    @Test
    public void standardSpacedTimeFormatterTest() {
        String formatted = TimeFormatter.STANDARD_SPACED.format(TimeUnit.NANOSECONDS, 10_050_012_050L, TimeUnit.SECONDS, TimeUnit.MICROSECONDS);
        Assert.assertEquals(formatted, "10 s, 50 ms, 12 us");
    }

}
