package me.taucu.tauutils;

import me.taucu.tauutils.chronology.Stopwatch;
import org.junit.Test;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class StopwatchTest {

    @Test
    public void testStopwatch() {
        Stopwatch w = Stopwatch.of();
        assertEquals(w.getName(), "unnamed timer");
        assertEquals(w.getElapsed(TimeUnit.NANOSECONDS), 0);
        LockSupport.parkNanos(10000);
        w.time();
        assertNotEquals(w.getElapsedNanos(), 0);
        w.pause();
        w.time();
        long pausedTime = w.getElapsedNanos();
        LockSupport.parkNanos(10000);
        w.time();
        assertEquals(pausedTime, w.getElapsedNanos());
        w.resume();
        w.time();
        assertNotEquals(w.getElapsedNanos(), pausedTime);
    }

}
