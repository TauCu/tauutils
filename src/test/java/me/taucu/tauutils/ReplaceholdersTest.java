package me.taucu.tauutils;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

public class ReplaceholdersTest {

    @Test
    public void testReplaceholders() {
        String test = "test %percent%\\% test %color_name% more \"%color_hex%\" test %percent%";

        HashMap<String, String> replacements = new HashMap<>();
        replacements.put("%color_name%", "RED");
        replacements.put("%percent%", "100.00");
        replacements.put("%color_hex%", "#FFFFFF");
        replacements.put("%required_percent%", "100.00");

        Assert.assertEquals(TextUtils.replaceholders(test, replacements), "test 100.00% test RED more \"#FFFFFF\" test 100.00");
    }

}
