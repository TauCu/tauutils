package me.taucu.tauutils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FileUtilsTest {

    @Test
    public void testFileUtils() {
        assertEquals("1.00TB", FileUtils.formatFilesize(1000000000000L));
        assertEquals("1.00TB", FileUtils.formatFilesize(999999999999L));
        assertEquals("1.85TB", FileUtils.formatFilesize(1850000000000L));

        assertEquals("1.00KB", FileUtils.formatFilesize(1000L));
        assertEquals("1.00KB", FileUtils.formatFilesize(999.999));
        assertEquals("1.85KB", FileUtils.formatFilesize(1850L));
    }

}
