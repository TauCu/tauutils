package me.taucu.tauutils;

import me.taucu.tauutils.chronology.TimeSplitter;
import me.taucu.tauutils.holders.Pair;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class TimeSplitterTest {

    @Test
    public void testTimeSplitter() {
        ArrayList<Pair<TimeUnit, Long>> units = new ArrayList<>();
        TimeSplitter.splitNanos(10_050_012_050L, TimeUnit.SECONDS, TimeUnit.MICROSECONDS, (tu, time) -> units.add(Pair.of(tu, time)));
        Assert.assertEquals(units.size(), 3);
        Assert.assertTrue(units.get(0).a() == TimeUnit.SECONDS && units.get(0).b() == 10);
        Assert.assertTrue(units.get(1).a() == TimeUnit.MILLISECONDS && units.get(1).b() == 50);
        Assert.assertTrue(units.get(2).a() == TimeUnit.MICROSECONDS && units.get(2).b() == 12);
    }

}
