package me.taucu.tauutils;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class RuntimeUtilsTest {

    @Test
    public void testRuntimeUtils() {
        Class<?> caller = RuntimeUtils.getCallerClass();
        assertTrue(caller.getName().contains("junit"));
    }

}
