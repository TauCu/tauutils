/*
 * Copyright (c) 2024 nullvoxel@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package me.taucu.tauutils.chronology;

import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;

public abstract class TimeSplitter {

    public static void splitNanos(long nanos, TimeUnit greater, TimeUnit lesser, BiConsumer<TimeUnit, Long> consumer) {
        int endId = toUnitId(greater);
        int startId = toUnitId(lesser);

        // days
        if (endId > 5) {
            if (startId <= 6 && (nanos / 86400000000000L > 0L || startId == 6)) {
                consumer.accept(TimeUnit.DAYS, nanos / 86400000000000L);
            }

            nanos %= 86400000000000L;
        }

        // hours
        if (endId > 4) {
            if (startId <= 5 && (nanos / 3600000000000L > 0L || startId == 5)) {
                consumer.accept(TimeUnit.HOURS, nanos / 3600000000000L);
            }

            nanos %= 3600000000000L;
        }

        // minutes
        if (endId > 3) {
            if (startId <= 4 && (nanos / 60000000000L > 0L || startId == 4)) {
                consumer.accept(TimeUnit.MINUTES, nanos / 60000000000L);
            }

            nanos %= 60000000000L;
        }

        // seconds
        if (endId > 2) {
            if (startId <= 3 && (nanos / 1000000000L > 0L || startId == 3)) {
                consumer.accept(TimeUnit.SECONDS, nanos / 1000000000L);
            }

            nanos %= 1000000000L;
        }

        // milliseconds
        if (endId > 1) {
            if (startId <= 2 && (nanos / 1000000L > 0L || startId == 2)) {
                consumer.accept(TimeUnit.MILLISECONDS, nanos / 1000000L);
            }

            nanos %= 1000000L;
        }

        // microseconds
        if (endId > 0) {
            if (startId <= 1 && (nanos / 1000L > 0L || startId == 1)) {
                consumer.accept(TimeUnit.MICROSECONDS, nanos / 1000L);
            }

            nanos %= 1000L;
        }

        // nanoseconds
        if (startId == 0) {
            consumer.accept(TimeUnit.NANOSECONDS, nanos);
        }
    }

    private static int toUnitId(TimeUnit src) {
        return switch (src) {
            case NANOSECONDS -> 0;
            case MICROSECONDS -> 1;
            case MILLISECONDS -> 2;
            case SECONDS -> 3;
            case MINUTES -> 4;
            case HOURS -> 5;
            case DAYS -> 6;
        };
    }

}
