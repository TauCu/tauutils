package me.taucu.tauutils.chronology;

import java.util.concurrent.TimeUnit;

public class Stopwatch {

    private String name = "unnamed timer";
    private long baseNanos = -1L;
    private long elapsedNanos = -1L;

    private long pausedNanos = -1L;

    /**
     * A simple timer that can be used for various things such as profiling
     * <p>
     * The default name is "unnamed timer"
     */
    public static Stopwatch of() {
        return new Stopwatch();
    }

    /**
     * A simple timer that can be used for various things such as profiling
     *
     * @param name used for identification via {@link #getName()}
     */
    public static Stopwatch of(String name) {
        return new Stopwatch(name);
    }

    /**
     * A simple timer that can be used for various things such as profiling
     * <p>
     * The default name is "unnamed timer"
     */
    public Stopwatch() {
        start();
    }

    /**
     * A simple timer that can be used for various things such as profiling
     *
     * @param name used for identification via {@link #getName()}
     */
    public Stopwatch(String name) {
        this.name = name;
    }

    /**
     * Starts the timer unpausing it if paused
     * <p>
     * If called again it will move the start time to the current time this method
     * is invoked
     */
    public void start() {
        pausedNanos = -1;
        baseNanos = System.nanoTime();
    }

    /**
     * Checks if the stopwatch is paused
     *
     * @return true if it is paused, false otherwise
     */
    public boolean isPaused() {
        return pausedNanos != -1;
    }

    /**
     * Pauses this stopwatch
     */
    public Stopwatch pause() {
        long cnanos = System.nanoTime();
        if (!isPaused()) {
            this.pausedNanos = cnanos;
        } else {
            throw new IllegalStateException("Stopwatch already paused");
        }
        return this;
    }

    /**
     * Resumes this watch from pause
     */
    public Stopwatch resume() {
        if (isPaused()) {
            baseNanos += (System.nanoTime() - pausedNanos);
        } else {
            throw new IllegalStateException("Stopwatch not paused");
        }
        return this;
    }

    /**
     * Saves the current elapsed time from when {@link #start()} was called for use
     * with {@link #getElapsedNanosAvg(double)} or another method
     */
    public Stopwatch time() {
        long cnanos = System.nanoTime();
        if (isPaused()) {
            elapsedNanos = pausedNanos - baseNanos;
        } else {
            elapsedNanos = cnanos - baseNanos;
        }
        return this;
    }

    /**
     * Resets this timer so that the elapsed time is now zero.
     * <p>
     * Does nothing if this timer was not started
     */
    public Stopwatch reset() {
        baseNanos = -1L;
        elapsedNanos = -1L;
        pausedNanos = -1;
        return this;
    }

    /**
     * Get the elapsed time in nanoseconds
     *
     * @return The elapsed time or 0 if {@link #start()} and {@link #time()} have
     * not been called or the timer has been {@link #reset()}
     */
    public long getElapsedNanos() {
        if (elapsedNanos < 0) {
            return 0;
        }
        return elapsedNanos;
    }

    /**
     * Gets the elapsed time of this timer
     *
     * @param unit the time unit
     * @return The elapsed time or 0 if {@link #start()} and {@link #time()} have
     * not been called or the timer has been {@link #reset()}
     */
    public long getElapsed(TimeUnit unit) {
        if (elapsedNanos < 0) {
            return 0;
        }
        return unit.convert(getElapsedNanos(), TimeUnit.NANOSECONDS);
    }

    /**
     * gets the elapsed time nanos divided by qty
     *
     * @param qty the divisor
     * @return the decimal average ns or 0 if {@link #start()} and {@link #time()}
     * have not been called or the timer has been {@link #reset()}
     */
    public double getElapsedAvg(TimeUnit unit, double qty) {
        return (unit.convert(elapsedNanos * 1_000_000, TimeUnit.NANOSECONDS) / 1_000_000D) / qty;
    }

    /**
     * gets the elapsed time nanos divided by qty
     *
     * @param qty the divisor
     * @return the decimal average ns or 0 if {@link #start()} and {@link #time()}
     * have not been called or the timer has been {@link #reset()}
     */
    public double getElapsedNanosAvg(double qty) {
        if (elapsedNanos < 0) {
            return 0;
        }
        return elapsedNanos / qty;
    }

    /**
     * Gets this timers name
     *
     * @return the timers name or "unnamed timer" if no name is set.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets the name of this timer
     *
     * @param name the name of this stopwatch
     */
    public void setName(String name) {
        this.name = name;
    }

}
