package me.taucu.tauutils;

import java.util.ArrayList;
import java.util.List;

public class EnumUtils {
    
    public static <T extends Enum<T>> T valueOf(Class<T> e, String s) {
        try {
            return Enum.valueOf(e, s);
        } catch (IllegalArgumentException a) {
            return null;
        }
    }

    public static List<String> enumToStringList(Enum<?>[] array) {
        List<String> strings = new ArrayList<String>();
        for (Enum<?> o : array) {
            strings.add(o.name());
        }
        return strings;
    }

    public static List<String> enumToStringList(Iterable<? extends Enum<?>> list) {
        List<String> strings = new ArrayList<String>();
        list.forEach(entry -> strings.add(entry.name()));
        return strings;
    }
    
}
