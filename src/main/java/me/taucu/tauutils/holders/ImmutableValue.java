package me.taucu.tauutils.holders;

public class ImmutableValue<V> extends Value<V> {

    public ImmutableValue(V v) {
        super(v);
    }

    @Override
    public void set(V value) {
        throw new UnsupportedOperationException();
    }

    public static <V> ImmutableValue<V> of(V v) {
        return new ImmutableValue<>(v);
    }

}
