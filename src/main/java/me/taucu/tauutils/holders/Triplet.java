package me.taucu.tauutils.holders;

import java.util.Objects;

public class Triplet<A, B, C> extends Pair<A, B> {

    protected C c;

    public Triplet() {
        super();
    }

    public Triplet(A a, B b, C c) {
        super(a, b);
        this.c = c;
        this.hashCode = 31 * this.hashCode + Objects.hashCode(c);
    }

    public C c() {
        return c;
    }

    public void c(C c) {
        this.c = c;
    }

    public static <A, B, C> Triplet<A, B, C> of(A a, B b, C c) {
        return new Triplet<>(a, b, c);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Triplet<?,?,?> that)) return false;

        return Objects.equals(a, that.a()) && Objects.equals(b, that.b()) && Objects.equals(c, that.c());
    }

    @Override
    public String toString() {
        return "Triplet{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                '}';
    }

}
