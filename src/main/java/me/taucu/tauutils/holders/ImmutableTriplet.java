package me.taucu.tauutils.holders;

public class ImmutableTriplet<A, B, C> extends Triplet<A, B, C> {

    public ImmutableTriplet(A a, B b, C c) {
        super(a, b, c);
    }

    @Override
    public void a(A a) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void b(B b) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void c(C c) {
        throw new UnsupportedOperationException();
    }

    public static <A, B, C> ImmutableTriplet<A, B, C> of(A a, B b, C c) {
        return new ImmutableTriplet<>(a, b, c);
    }

}
