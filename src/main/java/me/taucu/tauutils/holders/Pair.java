package me.taucu.tauutils.holders;

import java.util.Objects;

public class Pair<A, B> {
    
    protected A a;
    protected B b;
    protected int hashCode;

    public Pair() {
        this.hashCode = super.hashCode();
    }

    public Pair(A a, B b) {
        this.a = a;
        this.b = b;
        this.hashCode = Objects.hashCode(a);
        this.hashCode = 31 * this.hashCode + Objects.hashCode(b);
    }

    public A a() {
        return a;
    }

    public B b() {
        return b;
    }

    public void a(A a) {
        this.a = a;
    }

    public void b(B b) {
        this.b = b;
    }

    public static <A, B> Pair<A, B> of(A a, B b) {
        return new Pair<>(a, b);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pair<?, ?> that)) return false;

        return Objects.equals(a, that.a()) && Objects.equals(b, that.b());
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public String toString() {
        return "Pair{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }

}
