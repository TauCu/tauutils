package me.taucu.tauutils.holders;

import java.util.Objects;

public class Value<V> {

    protected V v;
    protected int hashCode;

    public Value() {
        hashCode = super.hashCode();
    }

    public Value(V v) {
        this.v = v;
        this.hashCode = Objects.hashCode(v);
    }

    public V get() {
        return v;
    }

    public void set(V v) {
        this.v = v;
    }

    public static <V> Value<V> of(V v) {
        return new Value<>(v);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Value<?> that)) return false;

        return Objects.equals(v, that.get());
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public String toString() {
        return "Value{" +
                "v=" + v +
                '}';
    }

}
