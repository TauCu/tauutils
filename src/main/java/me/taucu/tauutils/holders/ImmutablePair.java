package me.taucu.tauutils.holders;

public class ImmutablePair<A, B> extends Pair<A, B> {

    public ImmutablePair(A a, B b) {
        super(a, b);
    }

    @Override
    public void a(A a) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void b(B b) {
        throw new UnsupportedOperationException();
    }

    public static <A, B> ImmutablePair<A, B> of(A a, B b) {
        return new ImmutablePair<>(a, b);
    }

}
