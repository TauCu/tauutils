package me.taucu.tauutils;

import java.text.DecimalFormat;
import java.util.function.Function;

public class FileUtils {

    public static final DecimalFormat FILE_SIZE_FORMAT = new DecimalFormat("#.00");
    public static final char[] FILE_SIZE_MAGNITUDES = {'K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y', 'X'};

    /**
     * Converts Filesizes into readable text Ex: 1000 becomes 1.000kb, 100 becomes
     * 100b
     *
     * @param d Input filesize
     * @return Formatted String of input filesize
     */
    public static String formatFilesize(double d) {
        return formatFilesize(d, FILE_SIZE_FORMAT);
    }

    /**
     * Converts filesizes into readable text Ex: 1000.0 becomes 1.000kb, 100 becomes
     * 100b
     *
     * @param d  Input filesize
     * @param df Decimal Formatting
     * @return Formatted String of input filesize
     */
    public static String formatFilesize(double d, DecimalFormat df) {
        return formatFilesize(d, df, b -> {
            if (b == -1) {
                return "B";
            } else {
                return FileUtils.FILE_SIZE_MAGNITUDES[b] + "B";
            }
        });
    }

    /**
     * Converts filesizes into readable text Ex: 1000.0 becomes 1.000kb, 100 becomes
     * 100b
     *
     * @param d                  Input filesize
     * @param df                 Decimal Formatting
     * @param magnitudeFormatter function that will be invoked with a byte from -1 to {@link FileUtils#FILE_SIZE_MAGNITUDES}.length-1 used to format the filesize;
     *                           an example function would look like this:<br>
     * <pre>
     * {@code
     *formatFilesize(<filesize>, <decimalformat>, b -> {
     *     if (b == -1) {
     *         return "B";
     *     } else {
     * 	    return FileUtils.FILE_SIZE_MAGNITUDES[b] + "B";
     *     }
     *});
     * }
     * </pre>

     * @return Formatted String of input filesize
     */
    public static String formatFilesize(double d, DecimalFormat df, Function<Byte, String> magnitudeFormatter) {
        byte i = -1;
        while (d >= 999.999 && i < FILE_SIZE_MAGNITUDES.length) {
            d /= 1000;
            i++;
        }
        return df.format(d) + magnitudeFormatter.apply(i);
    }

}
