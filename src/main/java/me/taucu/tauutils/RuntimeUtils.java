package me.taucu.tauutils;

import java.lang.StackWalker.StackFrame;

public class RuntimeUtils {
    
    @SuppressWarnings("unchecked")
    public static <E extends Throwable> void sneakyThrow(Throwable e) throws E {
        throw (E) e;
    }
    
    public static Class<?> getCallerClass() {
        StackFrame f = getFrameAt(2);
        return f == null ? null : f.getDeclaringClass();
    }
    
    public static StackFrame getFrameAt(int previousFrames) {
        return StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE)
                .walk(s -> s.skip(previousFrames + 1).findFirst()).orElse(null);
    }
    
}
