package me.taucu.tauutils;

import java.util.Map;
import java.util.function.Function;

public class TextUtils {
    
    public static String replaceholders(String toReplace, Map<String, String> replacements) {
        return replaceholders(toReplace, found -> {
            String val = replacements.get(found);
            if (val != null) {
                return val;
            }
            return found;
        });
    }
    
    public static String replaceholders(String toReplace, Map<String, String> replacements, Function<String, String> onMissing) {
        return replaceholders(toReplace, found -> {
            String val = replacements.get(found);
            if (val != null) {
                return val;
            }
            return onMissing.apply(found);
        });
    }

    public static String replaceholders(String toReplace, Map<String, String> replacements, char opening, char closing) {
        return replaceholders(toReplace, found -> {
            String val = replacements.get(found);
            if (val != null) {
                return val;
            }
            return found;
        }, opening, closing);
    }

    public static String replaceholders(String toReplace, Map<String, String> replacements, Function<String, String> onMissing, char opening, char closing) {
        return replaceholders(toReplace, found -> {
            String val = replacements.get(found);
            if (val != null) {
                return val;
            }
            return onMissing.apply(found);
        }, opening, closing);
    }
    
    public static String replaceholders(String toReplace, Function<String, String> onFind) {
        return replaceholders(toReplace, onFind, '%', '%');
    }

    public static String replaceholders(String toReplace, Function<String, String> onFind, char opening, char closing) {
        int length = toReplace.length();

        int escapeCount = 0;
        int placeholderStart = -1;
        
        StringBuilder sb = new StringBuilder(length);
        int lastStartPos = 0;
        
        for (int index = 0; index < length; index++) {
            char at = toReplace.charAt(index);
            if (at == opening || at == closing) {
                if (at == opening && placeholderStart == -1) {
                    if (escapeCount % 2 == 0) {
                        placeholderStart = index;
                    } else {
                        sb.append(toReplace, lastStartPos, index - (escapeCount == 1 ? 1 : escapeCount / 2));
                        sb.append(at);
                        lastStartPos = index + 1;
                        escapeCount = 0;
                    }
                } else if (at == closing && placeholderStart != -1) {
                    if (lastStartPos < toReplace.length()) {
                        sb.append(toReplace, lastStartPos, placeholderStart - (escapeCount == 1 ? 1 : escapeCount / 2));
                    }

                    if (escapeCount % 2 == 0) {
                        sb.append(onFind.apply(toReplace.substring(placeholderStart, index + 1)));
                    } else {
                        sb.append(toReplace, placeholderStart, index + 1);
                    }

                    lastStartPos = index + 1;

                    placeholderStart = -1;
                    escapeCount = 0;
                }
            } else if (at == '\\') {
                escapeCount++;
            } else if (placeholderStart == -1) {
                escapeCount = 0;
            }
        }
        
        if (!toReplace.isEmpty() && lastStartPos < toReplace.length()) {
            sb.append(toReplace, lastStartPos, toReplace.length());
        }
        
        return sb.toString();
    }
    
}
